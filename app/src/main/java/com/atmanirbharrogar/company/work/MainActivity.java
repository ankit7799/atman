package com.atmanirbharrogar.company.work;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Patterns;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.view.ViewCompat;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

public class MainActivity extends AppCompatActivity {

    RelativeLayout r1, r2;
    ImageView login_icon, signup_icon;
    Handler h1 = new Handler();
    FirebaseAuth mAuth;
    DatabaseReference mDatabase, msubref; //for retrieving type of user
    TextInputEditText et_mail, et_pwd;
    TextInputLayout et_mailLayout, et_pwdLayout;
    ProgressDialog pd;
    Runnable run1 = new Runnable() {
        @Override
        public void run() {
            r1.setVisibility(View.VISIBLE);
            r2.setVisibility(View.VISIBLE);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        r1 = findViewById(R.id.relative_login);
        login_icon = findViewById(R.id.imgView_logo);
        signup_icon = findViewById(R.id.imgView_icon_signup);
        r2 = findViewById(R.id.relative_signup);
        et_mail = findViewById(R.id.login_mail);
        et_pwd = findViewById(R.id.login_pwd);
        et_mailLayout = findViewById(R.id.login_mailLayout);
        et_pwdLayout = findViewById(R.id.login_pwdLayout);
        pd = new ProgressDialog(this);

        //initialising firebase services
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            retrieve();
        } else
            h1.postDelayed(run1, 1500); //to make the layouts visible after 1.5 seconds giving an animation
    }

    public void login(View v) {
        //new checkInternetConnection(this).checkConnection();
        if (validation()) {
            et_mailLayout.setError(null);
            et_pwdLayout.setError(null);
            pd.setTitle("Please Wait");
            pd.setMessage("Logging in");
            pd.show();
            pd.setCanceledOnTouchOutside(false);

            mAuth.signInWithEmailAndPassword(et_mail.getText().toString(), et_pwd.getText().toString()).addOnCompleteListener(this, task -> {
                if (task.isSuccessful()) {
                    //Toast.makeText(MainActivity.this , "Success" , Toast.LENGTH_SHORT).show();
                    retrieve();
                    pd.dismiss();
                } else {
                    //Toast.makeText(MainActivity.this, "Sign In Failed", Toast.LENGTH_SHORT).show();
                    StyleableToast.makeText(this, "Sign In Failed", Toast.LENGTH_LONG, R.style.Maintoastred).show();

                    pd.dismiss();
                }
            });
        }



        ConnectivityManager managercheck =(ConnectivityManager)getApplicationContext().getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkcheck=managercheck.getActiveNetworkInfo();
        if(null!=activeNetworkcheck) {
            if (activeNetworkcheck.getType() == ConnectivityManager.TYPE_WIFI) {
                // Toast.makeText(MainActivity.this, "YOUR WIFI ON", Toast.LENGTH_SHORT).show();

                //StyleableToast.makeText(this,"Wifi Connected",Toast.LENGTH_LONG,R.style.Maintoastgreen).show();

            } else if (activeNetworkcheck.getType() == ConnectivityManager.TYPE_MOBILE) {
                //Toast.makeText(MainActivity.this, "YOUR MOBILE DATA ON", Toast.LENGTH_SHORT).show();
               // StyleableToast.makeText(this,"Mobile Data Connected",Toast.LENGTH_LONG,R.style.Maintoastgreen).show();
            }


        }
        else
        {
            // Toast.makeText(MainActivity.this, "NETWORK NOT AVAILABLE", Toast.LENGTH_SHORT).show();

            StyleableToast.makeText(this,"NETWORK NOT AVAILABLE",Toast.LENGTH_LONG,R.style.Maintoastred).show();

        }







    }

    public void retrieve() {
        FirebaseUser user = mAuth.getCurrentUser();
        msubref = mDatabase.child("Users").child(user.getUid());
        msubref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String type = dataSnapshot.child("Type").getValue(String.class);

                assert type != null;
                if (type.equals("Seeker")) {
                    startActivity(new Intent(MainActivity.this, SeekerMain.class));
                    finish();
                } else if (type.equals("Recruiter")) {
                    startActivity(new Intent(MainActivity.this, RecruiterMain.class));
                    finish();
                }
                //Toast.makeText(MainActivity.this , type , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public boolean validation() {
        //Toast.makeText(MainActivity.this , "Inside Validation" , Toast.LENGTH_SHORT).show();
        boolean valid = true;

        if (et_mail.getText().toString().isEmpty()) {
            et_mailLayout.setError("Email is required");
            et_mailLayout.requestFocus();
            valid = false;
        } else {
            et_mailLayout.setError(null);
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(et_mail.getText().toString()).matches()) {
            et_mailLayout.setError("Please Enter valid Email address");
            et_mailLayout.requestFocus();
            valid = false;
        } else {
            et_mailLayout.setError(null);
        }

        if (et_pwd.getText().toString().isEmpty()) {
            et_pwdLayout.setError("Please Enter the password");
            //et_pwdLayout.setErrorIconDrawable(null);
            et_pwdLayout.requestFocus();
            valid = false;
        } else {
            et_pwdLayout.setError(null);
        }

        if (et_pwd.getText().toString().length() < 6) {
            et_pwdLayout.setError("Minimum password length is 6");
            et_pwdLayout.requestFocus();
            valid = false;
        } else {
            et_pwdLayout.setError(null);
        }
        return valid;

    }

    public void signup(View v) //called when signup button is clicked
    {
        Intent shift_to_signup = new Intent(MainActivity.this, SignUpActivity.class);
        ActivityOptionsCompat actop = ActivityOptionsCompat.makeSceneTransitionAnimation(this, login_icon, ViewCompat.getTransitionName(login_icon));
        startActivity((shift_to_signup), actop.toBundle());
        overridePendingTransition(R.anim.fadein, R.anim.fadeout); //to replace default animations with fade in and fade out;
        //Snackbar.make(v , "Switch to Sign Up Page" , Snackbar.LENGTH_SHORT).show();
    }

    public void forgotpassword(View v) {
        startActivity(new Intent(MainActivity.this, ForgotPasswordModule.class));
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
